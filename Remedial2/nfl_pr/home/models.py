from django.db import models

#MODEL CITY
class Ciudad(models.Model):
    id_ciudad = models.AutoField(primary_key=True)
    name = models.CharField(max_length=80)

    def __str__(self):
        return self.name

#MODEL STADIUM
class Estadio(models.Model):
    id_estadio = models.AutoField(primary_key=True)
    name = models.CharField(max_length=80)

    def __str__(self):
        return self.name

#MODEL TEAM
class Equipo(models.Model):
    id_equipo = models.AutoField(primary_key=True)
    name = models.CharField(max_length=80)
    city = models.ForeignKey(Ciudad, on_delete=models.CASCADE, null=True)
    stadium = models.ForeignKey(Estadio, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name

