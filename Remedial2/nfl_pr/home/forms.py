from django import forms
from .models import Equipo, Ciudad, Estadio

#FORM EQUIPO
class UpdateEquipoForm(forms.ModelForm):
    city = forms.ModelChoiceField(queryset=Ciudad.objects.all(), required=False)
    stadium = forms.ModelChoiceField(queryset=Estadio.objects.all(), required=False)

    class Meta:
        model = Equipo
        fields = ['name', 'city', 'stadium']


class NewEquipoForm(forms.ModelForm):
    city = forms.ModelChoiceField(queryset=Ciudad.objects.all(), required=False)
    stadium = forms.ModelChoiceField(queryset=Estadio.objects.all(), required=False)

    class Meta:
        model = Equipo
        fields = ['name', 'city', 'stadium']


#FORM CIUDAD
class UpdateCiudadForm(forms.ModelForm):
    class Meta:
        model = Ciudad
        fields = ["id_ciudad", "name"]


class NewCiudadForm(forms.ModelForm):
    class Meta:
        model = Ciudad
        fields = ["id_ciudad", "name"]


#FORM ESTADIO
class UpdateEstadioForm(forms.ModelForm):
    class Meta:
        model = Estadio
        fields = ["id_estadio", "name"]

class NewEstadioForm(forms.ModelForm):
    class Meta:
        model = Estadio
        fields = ["id_estadio", "name"]
