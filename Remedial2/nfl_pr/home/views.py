from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.views import generic
from django.urls import reverse_lazy
from .models import Equipo, Ciudad, Estadio
from .forms import UpdateEquipoForm, NewEquipoForm, UpdateCiudadForm, NewCiudadForm, UpdateEstadioForm, NewEstadioForm

#VIEW EQUIPO

#DETAIL
class DetailEquipo(generic.DetailView):
    template_name = "home/detail_equipo.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "equipo": Equipo.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)


#DELETE
class DeleteEquipo(generic.DeleteView):
    template_name = "home/delete_equipo.html"
    model = Equipo
    success_url = reverse_lazy("home:equipoindex") 


#CREATE
class NewEquipo(generic.CreateView):
    template_name = 'home/new_equipo.html'
    model = Equipo
    form_class = NewEquipoForm
    success_url = reverse_lazy('home:teamindex')


#UPDATE
class UpdateEquipo(generic.UpdateView):
    template_name = 'home/update_equipo.html'
    model = Equipo
    form_class = UpdateEquipoForm
    success_url = reverse_lazy("home:teamindex")


#PLANTILLA HOME/TEAMINDEX
class TeamIndex(generic.View):
    template_name = "home/teamindex.html"
    context = {}

    def get(self, request):
        self.context = {
            "equipos": Equipo.objects.all()
        }
        return render(request, self.template_name, self.context)


#PLANTILLA BASE/EQUIPOINDEX
class EquipoIndex(generic.View):
    template_name = "base/equipoindex.html"
    context = {}

    def get(self, request):
        return render(request, self.template_name, self.context)


#///////////////////////////////////////////////////////////////////////////
#INDEX
class Index(generic.View):
    template_name = "base/lobby.html"
    context = {}

    def get(self, request):
        return render(request, self.template_name, self.context)
#//////////////////////////////////////////////////////////////////////////


#CIUDAD

#DETAIL
class DetailCiudad(generic.DetailView):
    template_name = "home/detail_ciudad.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "ciudad": Ciudad.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)


#DELETE
class DeleteCiudad(generic.DeleteView):
    template_name = "home/delete_ciudad.html"
    model = Ciudad
    success_url = reverse_lazy("home:ciudadindex") 


#CREATE
class NewCiudad(generic.CreateView):
    template_name = 'home/new_ciudad.html'
    model = Ciudad
    form_class = NewCiudadForm
    success_url = reverse_lazy('home:ciudadindex')


#UPDATE
class UpdateCiudad(generic.UpdateView):
    template_name = 'home/update_equipo.html'
    model = Ciudad
    form_class = UpdateCiudadForm
    success_url = reverse_lazy("home:ciudadindex")



#PLANTILLA HOME/TEAMINDEX
class CityIndex(generic.View):
    template_name = "home/cityindex.html"
    context = {}

    def get(self, request):
        self.context = {
            "ciudads": Ciudad.objects.all()
        }
        return render(request, self.template_name, self.context)


#///////////////////////////////////////////////////////////////////////////
#ESTADIO

#DETAIL
class DetailEstadio(generic.DetailView):
    template_name = "home/detail_estadio.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "estadio": Estadio.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)


#DELETE
class DeleteEstadio(generic.DeleteView):
    template_name = "home/delete_estadio.html"
    model = Estadio
    success_url = reverse_lazy("home:estadioindex") 


#CREATE
class NewEstadio(generic.CreateView):
    template_name = 'home/new_estadio.html'
    model = Estadio
    form_class = NewEstadioForm
    success_url = reverse_lazy('home:estadioindex')


#UPDATE
class UpdateEstadio(generic.UpdateView):
    template_name = 'home/update_estadio.html'
    model = Estadio
    form_class = UpdateEstadioForm
    success_url = reverse_lazy("home:estadioindex")



#PLANTILLA HOME/TEAMINDEX
class StadiumIndex(generic.View):
    template_name = "home/stadiumindex.html"
    context = {}

    def get(self, request):
        self.context = {
            "estadios": Estadio.objects.all()
        }
        return render(request, self.template_name, self.context)


#////////////////////////////////////////////////////////////////////////////////
#CONSULTAS QUE SE ENCUENTRAN EN EL MENU DE DESPLIEGUE DESDE LA PAGINA WEB


#Ciudades con equipos:
class CityWithTeams(generic.View):
    template_name = "home/citywithteams.html"
    context = {}

    def get(self, request):
        self.context = {
            "ciudades": Ciudad.objects.filter(equipo__isnull=False).distinct()
        }
        return render(request, self.template_name, self.context)


#Que equipo juega en que estadio:
class TeamStadium(generic.View):
    template_name = "home/teamstadium.html"
    context = {}

    def get(self, request):
        self.context = {
            "teams": Equipo.objects.select_related('stadium')
        }
        return render(request, self.template_name, self.context)


#Que equipos tienen una ciudad:
class CityTeams(generic.View):
    template_name = "home/cityteams.html"
    context = {}

    def get(self, request):
        self.context = {
            "ciudades": Ciudad.objects.exclude(equipo__isnull=True).distinct()
        }
        return render(request, self.template_name, self.context)